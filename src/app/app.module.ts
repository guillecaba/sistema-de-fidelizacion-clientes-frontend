import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsoComponent } from './pages/uso/uso.component';
import { CoreService } from './services/core.service';
import { APP_ROUTES } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { GastarComponent } from './pages/gastar/gastar.component';
import { BolsaComponent } from './pages/bolsa/bolsa.component';
import { NavbarComponent } from './shared/navbar/navbar.component';

import { FormsModule } from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './shared/footer/footer.component'
@NgModule({
  declarations: [
    AppComponent,
    UsoComponent,
    GastarComponent,
    BolsaComponent,
    NavbarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [CoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
