import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';

@Component({
  selector: 'app-uso',
  templateUrl: './uso.component.html',
  styleUrls: ['./uso.component.sass']
})
export class UsoComponent implements OnInit {
  usopuntos;
  clientes;
  conceptos;
  datos;
  constructor(private _coreService: CoreService ) { }

  ngOnInit() {
    this.datos = {
      'clienteId':'',
      'conceptoId':'',
      'fechaDesde':'',
      'fechaHasta':''
    }
      this._coreService.get_usopuntos(this.datos).subscribe(usopuntos => {
        console.log(usopuntos)
        this.usopuntos = usopuntos;
        this._coreService.get_clientes().subscribe(clientes => {
          this.clientes = clientes;
          this._coreService.get_conceptos().subscribe(conceptos => {
            this.conceptos = conceptos;
          })
        })
      })
  }

  filtrar() {
    console.log(this.datos)
    this._coreService.get_usopuntos(this.datos).subscribe(usopuntos => {
      this.usopuntos=usopuntos;
      console.log(this.usopuntos)
    });
  }

  resetear() {
    this.datos = {
      'clienteId':'',
      'conceptoId':'',
      'fechaDesde':'',
      'fechaHasta':''
    }
    this._coreService.get_usopuntos(this.datos).subscribe(usopuntos => {
      this.usopuntos=usopuntos;
    });
  }

}
