import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-gastar',
  templateUrl: './gastar.component.html',
  styleUrls: ['./gastar.component.scss']
})
export class GastarComponent implements OnInit {

  clientes:any;
  conceptos:any;
  submitted = false;

  constructor(private _coreService: CoreService) { 
   
   }

   gastarForm = new FormGroup({
    clienteId:new FormControl('',Validators.required),
    conceptoId: new FormControl('',Validators.required),
    cantidad:new FormControl('',Validators.required)
  });

  ngOnInit() {
    
    this._coreService.get_clientes().subscribe(clientes =>{
      this.clientes = clientes;
      this._coreService.get_conceptos().subscribe(conceptos => {
        console.log(conceptos);
        this.conceptos = conceptos;
      })
    })
  }


  onSubmit(form) {    
    console.log(form.value);
    this._coreService.post_gastar(form.value).subscribe(res => {
      console.log(res)
    })
}
  

 

}
