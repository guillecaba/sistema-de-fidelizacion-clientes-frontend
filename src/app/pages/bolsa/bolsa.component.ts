import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';

@Component({
  selector: 'app-bolsa',
  templateUrl: './bolsa.component.html',
  styleUrls: ['./bolsa.component.sass']
})
export class BolsaComponent implements OnInit {
  bolsas;
  clientes;
  estado;
  venceEn;
  clienteId
  datos;
 
  constructor(private _coreService:CoreService) { }

  ngOnInit() {
    this.datos = {
      'clienteId':'',
      'venceEn':'',
      'estado':''
    }
    this._coreService.get_bolsas(this.datos).subscribe(bolsas => {
      this.bolsas=bolsas;
      this._coreService.get_clientes().subscribe(clientes=>{
        this.clientes = clientes;
      })
    })
    
  }

  filtrar() {
    console.log(this.datos)
    this._coreService.get_bolsas(this.datos).subscribe(bolsas => {
      this.bolsas=bolsas;
    });
  }

  resetear() {
    this.datos = {
      'clienteId':'',
      'venceEn':'',
      'estado':''
    }
    this._coreService.get_bolsas(this.datos).subscribe(bolsas => {
      this.bolsas=bolsas;
    });
  }

}
