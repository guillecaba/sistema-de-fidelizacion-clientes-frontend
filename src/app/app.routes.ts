import { UsoComponent } from "./pages/uso/uso.component";
import { Routes, RouterModule } from '@angular/router'; 
import { GastarComponent } from './pages/gastar/gastar.component';
import { BolsaComponent } from './pages/bolsa/bolsa.component';



const appRoutes: Routes = [
    {path:'usopuntos',component:UsoComponent},
    {path:'gastar',component:GastarComponent},
    {path:'bolsa',component:BolsaComponent},
    {path:'',redirectTo:'/gastar',pathMatch:'full'},
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes);